'''
Created on 03.10.2013

@author: Solonarv
'''

from struct import Struct

ulonglong = Struct('>Q')
ushort = Struct('>H')